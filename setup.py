from pip.req import parse_requirements
from setuptools import setup, find_packages


install_reqs = parse_requirements('requirements.txt', session=False)
reqs = [str(ir.req) for ir in install_reqs]

setup(
    name='dedupy',

    version="0.1.2",

    packages=find_packages(),

    author="P. de Sahb, A. Giraud, LV. Pasquier",
    author_email="",

    description=open('README.md').read(),
    install_requires=reqs,

    url="https://gitlab.com/Dithyrambe/dedupy",

    classifiers=[
        "Programming Language :: Python :: 3.6",
    ],

    entry_points={
        'console_scripts': [

        ],
    },

    license="GPL"
)
