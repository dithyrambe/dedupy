#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    This module is highly inspired from dedupe (https://github.com/dedupeio/dedupe) 
    & libact (https://github.com/ntucllab/libact).
    It aims to perform fuzzy matching for record linkage / deduplication clustering.
"""

__version__ = '0.1.2'

import dedupy.active_learning
import dedupy.dataset
import dedupy.blocker
import dedupy.dataset
import dedupy.distances
import dedupy.feature_engineering

