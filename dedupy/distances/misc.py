from __future__ import absolute_import, unicode_literals

from .base import StringDistanceBase
from .utils import StringConvolve

import jellyfish as jf

__all__ = [
    'ExactDistance',
    'LongestCommonSubstring',
    'LengthDifferenceDistance',
    'LevenshteinDistance',
    'WordLevenshteinDistance',
    'ConvolutionDistance',
    'DigitLevenshteinDistance'
]


class ExactDistance(StringDistanceBase):
    """
    Return whether str1 and str2 are exactly identical (0.) or not (1.)
    """

    @staticmethod
    def _compute(str1, str2):
        """Return whether str1 and str2 are exactly identical (0.) or not (1.)"""
        res = float(str1 != str2)
        return res

    @staticmethod
    def _norm_factor(str1, str2):
        """No relevant normalization factor"""
        return 1.


class LongestCommonSubstring(StringDistanceBase):
    """
    Return the longest common substring's length
    """

    @staticmethod
    def _compute(str1, str2):
        """Return whether the length of the longest common substring shared by str1 & str2"""
        m = len(str1)
        n = len(str2)
        counter = [[0] * (n + 1) for _ in range(m + 1)]
        longest = 0.
        lcs_set = set()
        for i in range(m):
            for j in range(n):
                if str1[i] == str2[j]:
                    c = counter[i][j] + 1
                    counter[i + 1][j + 1] = c
                    if c > longest:
                        lcs_set = set()
                        longest = c
                        lcs_set.add(str1[i - c + 1:i + 1])
                    elif c == longest:
                        lcs_set.add(str1[i - c + 1:i + 1])
        return float(max(len(str1), len(str2))) - longest

    @staticmethod
    def _norm_factor(str1, str2):
        """Divide by length of longest string"""
        return 1. / float(max(len(str1), len(str2), 1))


class LengthDifferenceDistance(StringDistanceBase):
    """Return length difference of 2 strings"""

    @staticmethod
    def _compute(str1, str2):
        """Return length difference of 2 strings"""
        return float(abs(len(str1) - len(str2)))

    @staticmethod
    def _norm_factor(str1, str2):
        """Compare to longest"""
        return 1. / max(len(str1), len(str2), 1)


class LevenshteinDistance(StringDistanceBase):
    """Return levenshtein distance between str1 & str2"""

    @staticmethod
    def _compute(str1, str2):
        """Levenshtein from jellyfish"""
        return float(jf.levenshtein_distance(str1, str2))

    @staticmethod
    def _norm_factor(str1, str2):
        """Divide by length of longest string"""
        return 1. / max(len(str1), len(str2), 1)


class WordLevenshteinDistance(StringDistanceBase):
    """Return sum of words pairwise minimal distance """

    @staticmethod
    def _compute(str1, str2):
        """Computes sum of pairwised minimum levenshtein distance (per word)"""
        words1 = str1.split(' ')
        words2 = str2.split(' ')
        levenshteins = [[jf.levenshtein_distance(w1, w2) for w2 in words2] for w1 in words1]
        lev_sum1 = sum(map(min, levenshteins))
        lev_sum2 = sum(map(min, zip(*levenshteins)))
        return 0.5 * (lev_sum1 + lev_sum2)

    @staticmethod
    def _norm_factor(str1, str2):
        """Divide by maximum word times number of words per string"""
        l1 = len(str1.split(' '))
        l2 = len(str2.split(' '))
        longest1 = max(1, *map(len, str1.split(' ')))
        longest2 = max(1, *map(len, str2.split(' ')))
        return 2. / (max(longest1, longest2) * (l1 + l2))


class ConvolutionDistance(StringDistanceBase):
    """Compute string convolution between str1 & str2"""

    @staticmethod
    def _compute(str1, str2):
        """Max pooling of convolution"""
        convolv_res = StringConvolve.convolve(str1, str2)
        return max(len(str1), len(str2)) - max(convolv_res)

    @staticmethod
    def _norm_factor(str1, str2):
        """Divide by longest length of str1 & str2"""
        return 1. / max(len(str1), len(str2), 1)


class DigitLevenshteinDistance(StringDistanceBase):
    """Compute Levenshtein distance only on ordered digit of the strings"""

    @staticmethod
    def _compute(str1, str2):
        return LevenshteinDistance._compute(str1, str2)

    @staticmethod
    def _substrings(str1, str2):
        sub1 = ''.join([s for s in str1 if s.isdigit()])
        sub2 = ''.join([s for s in str2 if s.isdigit()])
        return sub1, sub2

    @staticmethod
    def _norm_factor(str1, str2):
        sub1, sub2 = DigitLevenshteinDistance._substrings(str1, str2)
        return LevenshteinDistance._norm_factor(sub1, sub2)
