from six import with_metaclass, string_types

import abc

__all__ = [
    'StringDistanceBase',
]


class StringDistanceBase(with_metaclass(abc.ABCMeta)):
    """
    Distance class
    """

    MODES = ['raw', 'norm']
    FORMATS = {'dict': dict,
               'tuple': tuple,
               'list': list}

    def __init__(self, modes='all', out_format='dict'):
        """
        Constructor
        :param modes: Define the ordered tuple that instance call will return.
        Take values in ['raw', 'normalized', 'all'].
        'raw': raw distance
        'norm': raw * norm factor to retrieve output between 0. & 1.
        'all': both raw & norm
        """
        self.cache = dict()  # will contain all computed values to avoid duplicated computations
        self.out_format = out_format

        if isinstance(modes, string_types):
            if modes == 'all':
                self.modes = StringDistanceBase.MODES
            else:
                self.modes = [modes]

        elif isinstance(modes, list):
            self.modes = modes

        else:
            raise TypeError("modes must be instance of str or list")

        assert all(map(lambda x: x in StringDistanceBase.MODES, self.modes)), "modes must be in " \
                                                                              "{}".format(StringDistanceBase.MODES)
        assert out_format in StringDistanceBase.FORMATS, "format must be un {}".format(
            StringDistanceBase.FORMATS.keys())

    def __call__(self, str1, str2):
        """
        call _compute method
        :param str1:
        :param str2:
        :return:
        """
        keys, values = zip(*((mode, getattr(self, mode)(str1, str2)) for mode in self.modes))  # generator for outputs
        if self.out_format == 'dict':
            res = dict(zip(keys, values))
        else:
            res = self.FORMATS[self.out_format](values)
        self.cache = dict()  # empty cash
        return res

    @staticmethod
    def _substrings(str1, str2):
        """
        Get the 2-uple of substrings of interest (outputed from regex for example)
        :param str1:
        :param str2:
        :return:
        """
        return str1, str2

    @staticmethod
    @abc.abstractmethod
    def _compute(str1, str2):
        """
        Compute the raw string distance between str1 & str2
        :return: float
        """
        return 0.

    @staticmethod
    @abc.abstractmethod
    def _norm_factor(str1, str2):
        """
        Return the coefficient for normalization of the raw distance (to scale between 0. & 1.)
        :return: float
        """
        return 1.

    def raw(self, str1, str2):
        """
        Compute and fill cache with raw distance as the 'dist' key
        :param str1:
        :param str2:
        :return: the raw 'dist' from cache
        """
        strings = self.cache['strings'] = self.cache.get('strings') or self._substrings(str1, str2)
        dist = self.cache['dist'] = self.cache.get('dist') or self._compute(*strings)
        return dist

    def norm(self, str1, str2):
        """
        Compute and fill cache with normalization coefficient as the 'norm' key
        :param str1:
        :param str2:
        :return: the 'norm' coefficient from cache
        """
        strings = self.cache['strings'] = self.cache.get('strings') or self._substrings(str1, str2)
        dist = self.cache['dist'] = self.cache.get('dist') or self._compute(*strings)
        norm = self.cache['norm'] = self.cache.get('norm') or self._norm_factor(*strings)
        return dist * norm

    @property
    def __name__(self):
        return self.__class__.__name__
