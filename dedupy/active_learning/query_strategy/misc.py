from __future__ import absolute_import

import random

import numpy as np

from .base import QueryStrategy


__all__ = [
    'RandomMinMax',
    'RandomTopFlop',
    'RandomSampling',
    'SklearnBoundaryStrategy',
]


class RandomMinMax(QueryStrategy):
    """Query strategy pushing min or max of a random feature in the given subset of features"""

    def __init__(self, subset='all'):
        assert isinstance(subset, list) or subset == 'all', "subset must be list of integer or 'all'"
        self.subset = subset

    def make_query(self, dataset):
        unlabeled_entry_ids, X = dataset.get_unlabeled_entries()
        nb_features = X.shape[1]

        if self.subset == 'all':
            feature = random.randint(0, nb_features - 1)
        else:
            feature = random.choice(self.subset)

        score = X[:, feature]
        mode = random.choice(['min', 'max'])

        ask_id = None
        if mode == 'min':
            ask_id = score.argmin()

        elif mode == 'max':
            ask_id = score.argmax()

        return unlabeled_entry_ids[ask_id]


class RandomTopFlop(QueryStrategy):
    """Query strategy pushing """

    def __init__(self, quantile=0.15, subset='all', mode='random'):
        assert isinstance(subset, list) or subset == 'all', "subset must be list of integer or 'all'"
        assert mode in ['top', 'flop', 'random'], "mode must be 'random', 'top' or 'flop' (default='random')."

        self.quantile = quantile
        self.percentile = round(100. * quantile)
        self.subset = subset
        self.mode = mode

    def make_query(self, dataset):
        unlabeled_entry_ids, X = dataset.get_unlabeled_entries()
        nb_features = X.shape[1]

        if self.subset == 'all':
            feature = random.randint(0, nb_features - 1)
        else:
            feature = random.choice(self.subset)
        score = X[:, feature]

        if self.mode == 'random':
            mode = random.choice(['top', 'flop'])
        else:
            mode = self.mode

        ask_id = None
        if mode == 'top':
            top_filter = score > np.percentile(score, 100 - self.percentile)
            if any(top_filter):
                ask_id = random.choice(np.where(top_filter)[0])
            else:
                return random.choice(range(len(score)))

        elif mode == 'flop':
            flop_filter = score < np.percentile(score, self.percentile)
            if any(flop_filter):
                ask_id = random.choice(np.where(flop_filter)[0])
            else:
                return random.choice(range(len(score)))

        return unlabeled_entry_ids[ask_id]


class RandomSampling(QueryStrategy):
    """Query strategy pushing an valid unlabeled indice randomly"""

    def __init__(self):
        pass

    def make_query(self, dataset):
        unlabeled_entry_ids, _ = dataset.get_unlabeled_entries()
        ask_id = random.randint(0, len(unlabeled_entry_ids))
        entry_id = unlabeled_entry_ids[ask_id]
        return entry_id


class SklearnBoundaryStrategy(QueryStrategy):
    def __init__(self, model, max_sample_size=None):
        assert hasattr(model, 'fit') and hasattr(model,
                                                 'predict_proba'), "sklearn model must have a fit & a predict_proba method"
        self.model = model
        self.max_sample_size = max_sample_size

    def make_query(self, dataset):
        unlabeled_entry_ids, X = dataset.get_unlabeled_entries()
        _, train_X, train_y = dataset.get_labeled_entries()
        self.model.fit(train_X, train_y)

        if self.max_sample_size and len(unlabeled_entry_ids) > self.max_sample_size:
            random_indices = np.random.choice(len(unlabeled_entry_ids), self.max_sample_size, replace=False)
            unlabeled_entry_ids = unlabeled_entry_ids[random_indices]
            X = X[random_indices]

        score = self.model.predict_proba(X)[:, -1]
        distance_to_boundary = np.abs(score - 0.5)
        ask_id = distance_to_boundary.argmin()

        return unlabeled_entry_ids[ask_id]