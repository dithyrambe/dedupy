from __future__ import absolute_import

import random

from .base import QueryStrategy

__all__ = [
    'QueryStrategyEnsemble',
]


class QueryStrategyEnsemble(QueryStrategy):
    def __init__(self, query_strategies):
        for qs in query_strategies:
            assert isinstance(qs, QueryStrategy), "query_strategies must only contains QueryStrategy types"
        self.strategy_pool = query_strategies

    def make_query(self, dataset):
        strat = self.pick_strategy()
        return strat.make_query(dataset)

    def pick_strategy(self):
        return random.choice(self.strategy_pool)
