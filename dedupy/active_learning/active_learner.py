from __future__ import absolute_import

import pickle

import numpy as np

from .labeler import Labeler

__all__ = [
    'ActiveLearner'
]


class ActiveLearner:
    def __init__(self, nb_rounds):
        self.nb_rounds = nb_rounds
        self.features = None
        self.labels = None

    def learn(self, dataset, query_strategy, append_after_learn=False):
        labeler = Labeler(dataset, dataset.featuring_way)

        for _ in range(self.nb_rounds):
            query_id = query_strategy.make_query(dataset)
            lbl = labeler.label(query_id)
            dataset.update(query_id, lbl)

        if append_after_learn:
            self.append_training_datas(dataset)
        return self

    def set_nb_rounds(self, nb_rounds):
        self.nb_rounds = nb_rounds
        return self

    def get_sklearn(self, dataset, sklearn_instance):
        ids, X, y = dataset.get_labeled_entries()
        sklearn_instance.fit(X, y)
        return sklearn_instance

    def dump_model(self, model, path):
        with open(path, 'wb') as f:
            pickle.dump(model, f)

    def append_training_datas(self, dataset):
        _, X, labels = dataset.get_labeled_entries()

        if self.features is None:
            self.features = X
            self.labels = labels
        else:
            self.features = np.concatenate((self.features, X))
            self.labels = np.concatenate(self.labels, labels)
