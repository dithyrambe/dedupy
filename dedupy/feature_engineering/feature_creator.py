import os
import tempfile
import shutil

from functools import partial
import multiprocessing as mp

import numpy as np

from joblib import Parallel, delayed, dump

__all__ = [
    'FeatureCreator'
]


def _transform_single(args, left=None, right=None, res=None, features=None):
    k, (ileft, iright) = args
    # Stock des index des lignes comparees pour les retrouver facilement
    actual_features = []
    for descriptor in features:
        comparator = descriptor['comparator']
        feats = comparator(left[ileft, descriptor['left_on']], right[iright, descriptor['right_on']])
        if isinstance(feats, dict):
            for mode in comparator.modes:
                actual_features.append(feats[mode])
        else:
            for feat in feats:
                actual_features.append(feat)
    res[k] = actual_features
    return None


def _transform_single_no_memmap(args, left=None, right=None, features=None):
    ileft, iright = args
    actual_features = []
    for descriptor in features:
        comparator = descriptor['comparator']
        feats = comparator(left[ileft, descriptor['left_on']], right[iright, descriptor['right_on']])
        if isinstance(feats, dict):
            for mode in comparator.modes:
                actual_features.append(feats[mode])
        else:
            for feat in feats:
                actual_features.append(feat)
    return actual_features


class ParallelSetup:
    def __init__(self, features, dataset):
        self.features = features
        self.output_shape = self._get_output_shape(features)
        self.tmp_folder = tempfile.mkdtemp()
        tmp_left = os.path.join(self.tmp_folder, 'left.mmap')
        tmp_right = os.path.join(self.tmp_folder, 'right.mmap')
        tmp_res = os.path.join(self.tmp_folder, 'res.mmap')

        dump(dataset.left, tmp_left)
        self.leftm = np.memmap(tmp_left, dtype=np.object, shape=dataset.left.shape, mode='w+')
        self.leftm[:] = dataset.left

        dump(dataset.right, tmp_right)
        self.rightm = np.memmap(tmp_right, dtype=np.object, shape=dataset.right.shape, mode='w+')
        self.rightm[:] = dataset.right

        indices = dataset.indices

        res_shape = (indices.shape[0], self.output_shape)

        self.res = np.memmap(tmp_res, dtype=np.float64, shape=res_shape, mode='w+')

    def __enter__(self):
        """For 'with' statement"""
        return self.leftm, self.rightm, self.res

    def __exit__(self, type, value, traceback):
        """Make sure to remove tempfiles"""
        shutil.rmtree(self.tmp_folder)

    @staticmethod
    def _get_output_shape(features):
        """Return the shape of the output"""
        return sum((len(feature['comparator'].modes) for feature in features))


class FeatureCreator:
    def __init__(self, features, n_jobs=1):
        """
        Constructor
        :param features: Columns and comparator in the following format :
        [
            {
                'left_on': 'LEFTNAME',
                'right_on': 'RIGHTNAME',
                'comparator': LevenshteinDistance(modes='all')
            },
            {
                'on': 'COMMONNAME',
                'comparator': LevenshteinDistance(modes='all')
            }
        ]
        :param n_jobs: Number of thread
        """
        assert self._validate_features(features)
        self.features = features
        self.n_jobs = n_jobs
        self.column_names = None
        self._translated_features = None

        self.fitted = False

    def fit(self, dataset):
        self._translate_features(dataset)
        self.fitted = True

    def transform(self, dataset):
        """
        Process features conversion
        :param left: left data_frame
        :param right: right_dataframe (reference)
        :param indices: relevant indices where computation is needed
        :param output_type: if 'array' numpy, if 'df' or 'dataframe' pandas.DataFrame
        :return: indices, features. First columns stands for left ids, second columns stands for right ids
        """
        assert self.fitted, "FeatureCreator must be fitted before transformation. Call FeatureCreator.fit"

        comparison = dataset.indices

        with ParallelSetup(self.features, dataset) as (left, right, res):
            _multi_transform = partial(_transform_single_no_memmap,
                                       left=left, right=right, features=self._translated_features)

            pool = mp.Pool(self.n_jobs)
            res = pool.map(_multi_transform, comparison)
            pool.close()
            # delayed_comp = delayed(_transform_single)
            # _ = Parallel(n_jobs=self.n_jobs, pre_dispatch='all')(
            #     delayed_comp((k, (i, j)), left, right, res, self._translated_features) for k, (i, j) in
            #     enumerate(comparison))

            features = np.array(res)

            dataset.set_as_ready(features, self.features)

        return dataset

    def fit_transform(self, dataset):
        self.fit(dataset)
        return self.transform(dataset)

    def _translate_features(self, dataset):
        """Return features name in order to retrieve which features match which distance/columns couple"""
        left_cols, right_cols = dataset.left_columns, dataset.right_columns
        self._translated_features = list()
        self.column_names = list()
        for descriptor in self.features:
            res = {'comparator': descriptor['comparator']}
            if 'on' in descriptor:
                res['left_on'] = left_cols.index(descriptor['on'])
                res['right_on'] = right_cols.index(descriptor['on'])
                for mode in descriptor['comparator'].modes:
                    self.column_names.append('_'.join(
                        [descriptor['on'], descriptor['on'], descriptor['comparator'].__name__, mode]))
            else:
                for mode in descriptor['comparator'].modes:
                    self.column_names.append('_'.join(
                        [descriptor['left_on'], descriptor['right_on'], descriptor['comparator'].__name__, mode]))
                res['left_on'] = left_cols.index(descriptor['left_on'])
                res['right_on'] = right_cols.index(descriptor['right_on'])
            self._translated_features.append(res)

    @staticmethod
    def _validate_features(features):
        """Check if input features respect the correct data structure"""
        for descriptor in features:
            if 'comparator' not in descriptor:
                return False
            if 'on' in descriptor:
                if len(descriptor) != 2:
                    return False
            elif not (('left_on' in descriptor) and ('right_on' in descriptor)):
                return False
        return True
