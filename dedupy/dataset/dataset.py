import itertools
import operator
import warnings
from copy import deepcopy

import numpy as np
import scipy.sparse as sp

__all__ = [
    'Dataset',
]


class Dataset:
    def __init__(self, left, right=None):
        self.left = left.values
        self.left_columns = list(left.columns)
        if right is None:
            self.left_is_right = True
            self.right = self.left
            self.right_columns = self.left_columns
        else:
            self.left_is_right = False
            self.right = right.values
            self.right_columns = list(right.columns)

        # Attributes useful for active learning
        self._ids = None
        self._indices = None
        self._scores = None
        self._features = None
        self._labels = None
        self._featuring_way = None

        # Attributes useful to describe the different states of the dataset instance
        self._is_blocked = False
        self._is_ready = False
        self._is_matched = False

        # golden attributes
        self._matches = None
        self._matches_scores = None

        self._graph = None
        self._memberships = None
        self._canon = None

    def set_as_blocked(self, indices, scores):
        """
        Set the Dataframe as blocked
        :param indices: {array_like}(n_left_sample, m_right_sample). Indices to compare pairwise.
        :param scores: {array_like}(n_left_sample, m_right_sample). Scores of the comparison relevancy (the bigger, the
        more relevant)
        :return:
        """
        self._indices = self.get_comparison_indices(indices)
        self._scores = scores
        self._is_blocked = True

    def set_as_ready(self, features, featuring_way):
        """
        Set the Dataframe as ready for active learning
        :param features: {array_like}(n_left_sample * m_right_sample, n_features). Numerical descriptors for the couples
        :param featuring_way: list of dict. features passed to FeatureCreator
        :return:
        """
        self._featuring_way = featuring_way
        self._features = features
        self._labels = np.repeat(np.nan, features.shape[0])
        self._ids = np.arange(features.shape[0])
        self._is_ready = True

    @property
    def indices(self):
        if self.is_blocked:
            return self._indices
        else:
            return self.get_comparison_indices(indices=None)

    @property
    def scores(self):
        assert self._is_blocked, "dataset has not been blocked yet. Use Blocker.transform(dataset)"
        return self._scores

    @property
    def features(self):
        assert self._is_ready, "dataset has not been featured yet. Use FeatureCreator.transform(dataset)"
        return self._features

    @property
    def is_blocked(self):
        return self._is_blocked

    @property
    def is_ready(self):
        return self._is_ready

    @property
    def featuring_way(self):
        assert self._is_ready, "dataset has not been featured yet. Use FeatureCreator.transform(dataset)"
        return self._featuring_way

    @property
    def matches(self):
        return self._matches

    @property
    def matches_scores(self):
        return self._matches_scores

    @property
    def graph(self):
        assert self._graph is not None, "No linkage done yet. Try Dataset.get_graph_from"
        return self._graph

    @property
    def memberships(self):
        assert self._memberships is not None, "No linkage done yet. Try Dataset.link"
        return self._memberships

    @property
    def canon(self):
        assert self._canon is not None, "Must use .link with canonicalize set to True"
        return self._canon

    # ----------------------------------
    # implements the libact-like methods
    # ----------------------------------

    def get_unlabeled_entries(self):
        """
        Return all unlabeled entries
        :return: ids & features (in sklearn format)
        """
        nan_filter = np.isnan(self._labels)
        return self._ids[nan_filter], self._features[nan_filter]

    def get_labeled_entries(self):
        """
        Return all labeled entries
        :return: ids, features (in sklearn format) & labels
        """
        non_nan_filter = ~np.isnan(self._labels)
        ids = self._ids[non_nan_filter]
        features = self._features[non_nan_filter]
        labels = self._labels[non_nan_filter]
        return ids, features, labels

    def get_entries(self):
        """
        Return all entries
        :return: ids, features & labels
        """
        return self._ids, self._features, self._labels

    def update(self, entry_id, label):
        """
        Update Dataset to label one example
        :param entry_id: id of the example
        :param label: label to store
        :return:
        """
        self._labels[entry_id] = label

    def on_update(self, callback):
        """This can be implemented in order to update some other object of the API"""
        pass

    def get_query_from(self, query_strategy):
        """
        Given a QueryStragy, return the indice of the
        :param query_strategy:
        :return: indices of query to look at
        """
        return query_strategy.make_query(self)

    # -----
    # Utils
    # -----

    def translate_parameters(self, parameters):
        """Return features name in order to retrieve which features match which distance/columns couple"""
        translated_parameters = list()
        for descriptor in parameters:
            res = deepcopy(descriptor)
            if 'on' in res:
                res['left_on'] = self.left_columns.index(res['on'])
                res['right_on'] = self.right_columns.index(res['on'])
                del res['on']
            else:
                res['left_on'] = self.left_columns.index(res['left_on'])
                res['right_on'] = self.right_columns.index(res['right_on'])

            translated_parameters.append(res)

        return translated_parameters

    def get_comparison_indices(self, indices=None):
        """Get 2-uple of left & right matches"""
        if indices is not None:
            res = indices
        else:
            warnings.warn("dataset has not been blocked yet, all pairwise comparaison will be yield")
            res = np.array(list(itertools.product(range(self.left.shape[0]), range(self.right.shape[0]))))
        if self.left_is_right:
            res = np.array([(i, j) for i, j in res if i > j])
        return res

    def get_matches_from(self, model):
        assert self._is_ready, "Dataset is not ready to be match."
        assert hasattr(model, 'predict_proba'), "model must have a 'predict_proba' method"

        scores = model.predict_proba(self._features)[:, 1]
        tmp = np.hstack((self.indices, scores.reshape((len(scores), 1))))
        matches = -np.ones((len(self.left), 3))  # default values if no matches
        matches[:, 0] = range(len(self.left))
        for (row, possible_matches) in itertools.groupby(tmp, key=operator.itemgetter(0)):
            best_match = sorted(possible_matches, key=operator.itemgetter(2), reverse=True)[0]
            matches[int(row), :] = best_match

        self._matches = matches[:, :2].astype('int64')
        self._matches_scores = matches[:, 2]

        return self.matches, self.matches_scores

    def get_graph_from(self, model):
        """Return the similarity graph as a scipy.sparse.csr matrix"""
        assert self._is_ready, "Dataset is not ready to be match."
        assert hasattr(model, 'predict_proba'), "model must have a 'predict_proba' method"

        scores = model.predict_proba(self._features)[:, 1]

        graph = sp.lil_matrix((len(self.left), len(self.right)))
        graph[self.indices[:, 0], self.indices[:, 1]] = scores

        self._graph = graph.tocsr()
        return self._graph

    def link(self, model, tol=0.8, canonicalize=False):
        assert self._is_ready, "Dataset is not ready to be match."
        assert hasattr(model, 'predict_proba'), "model must have a 'predict_proba' method"

        graph = self.get_graph_from(model)
        nb_cluster, memberships = sp.csgraph.connected_components(graph > tol)

        if canonicalize:
            canon = {}
            for clst in range(nb_cluster):
                where_clust = np.where(memberships == clst)[0]
                lines, columns = zip(*itertools.product(where_clust, where_clust))
                submat = np.array(graph[lines, columns].reshape(where_clust.shape[0], where_clust.shape[0]))
                canonical_score = submat.sum(axis=0) + submat.sum(axis=1)
                best_canon = canonical_score.argmax()
                canon[clst] = best_canon
            self._canon = canon

        self._memberships = memberships
        return nb_cluster, memberships
