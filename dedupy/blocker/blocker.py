"""
blocker.py define a class for string blocking
"""
import numpy as np


__all__ = [
    'Blocker',
]


class Blocker:
    def __init__(self, blocks, top=None, min_score=None, n_jobs=1):
        """
        Constructor
        :param blocks: list of dict in the following format
            [
                {
                    'left_on': 'LEFT_FIELD',
                    'right_on': 'RIGHT_FIELD',
                    'weight': 2,
                    'vectorizer': CountVectorizer()
                },
                {
                    'on': 'COMMON_FIELD',
                    'weight': 1,
                    'vectorizer': CountVectorizer()
                }
            ]
        where:
        'left_on', 'right_on', 'on' are the names of the columns to compare pairwise
        'weight' is the weight use for relative weighting between columns blocking
        'vectorizer' is the instance of Vectorizer to use for blocking the considered columns (cf. sklearn)
        :param top: the number of maximum best candidate to return into right rows for each left rows
        :param n_jobs: number of thread for joblib (multiprocessing)
        """
        assert self._check_args_validity(blocks), "you must respect the following scheme for blocks arg :" \
                                                  """[
                {
                    'left_on': 'LEFT_FIELD',
                    'right_on': 'RIGHT_FIELD',
                    'weight': 2,
                    'vectorizer': CountVectorizer()
                },
                {
                    'on': 'COMMON_FIELD',
                    'weight': 1,
                    'vectorizer': CountVectorizer()
                }
            ]"""
        assert top is None or top > 0, "top is not suitable"

        self.blocks = blocks
        self.translated_blocks = None
        self.fitted = False
        self.top = top
        self.min_score = min_score or -1
        self.n_jobs = n_jobs

    def fit(self, dataset):
        """Fit all Vectorizer"""
        self.translated_blocks = dataset.translate_parameters(self.blocks)
        for block in self.translated_blocks:
            block['vectorizer'].fit(dataset.left[:, block['left_on']])

        # set the suitable self.to_keep attribute
        if self.top is None:
            self.to_keep = len(dataset.right)
        elif isinstance(self.top, int):
            self.to_keep = self.top
        elif isinstance(self.top, float):
            self.to_keep = int(len(dataset.right) * self.top)
        else:
            assert False, "top is not suitable"

        self.fitted = True

    @staticmethod
    def _multiply_single(dataset, left_on=None, right_on=None, vectorizer=None, weight=None):
        """
        Given a weight, return a dot product matrix * weight
        The dot product is supposed to return the number of common ngram between line i of left and line j of right
        """

        t_left = vectorizer.transform(dataset.left[:, left_on])
        if dataset.left_is_right and (left_on == right_on):
            t_right = t_left
        else:
            t_right = vectorizer.transform(dataset.right[:, right_on])
        return weight * (t_left.dot(t_right.T))

    def _multiply_all(self, dataset):
        """Return the weighted sum result of blocking matrices"""
        matrices = (self._multiply_single(dataset, **block) for block in self.translated_blocks)
        return sum(matrices)

    def _process_result_matrix(self, matrix):
        matrix = matrix.toarray()
        arg_sorted = (-matrix).argsort(axis=1)
        tol_fiter = matrix > self.min_score

        indices = []
        for row, order, f in zip(matrix, arg_sorted, tol_fiter):
            indices.append(order[f[order]][:self.to_keep])

        reshaped_indices = np.array([(i, j) for i in range(len(indices)) for j in indices[i]])
        scores = np.array([matrix[line, col] for line, col in reshaped_indices])
        return reshaped_indices, scores

    def transform(self, dataset):
        """Return the best top indices & top scores"""
        assert self.fitted, "Blocker must be fitted before transformation. Call Blocker.fit"
        
        result_matrix = self._multiply_all(dataset)
        indices, scores = self._process_result_matrix(result_matrix)
        dataset.set_as_blocked(np.array(indices), np.array(scores))
        return dataset

    def fit_transform(self, dataset):
        """Apply fit then transform given left & right"""
        self.fit(dataset)
        return self.transform(dataset)

    @staticmethod
    def _check_args_validity(blocks):
        """Check validity of the positional argument"""
        for block in blocks:
            if 'vectorizer' not in block:
                return False
            if 'weight' not in block:
                return False
            if 'on' not in block:
                if 'left_on' not in block or 'right_on' not in block:
                    return False
        return True
