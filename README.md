# HOW TO ?

Load raw datas:
```python
import pandas as pd
to_link = pd.read_csv('path/to/data/to_merge.csv')
reference = pd.read_csv('path/to/data/reference.csv')
```

Define the Dataset (core interface)
```python
from deduptools.dataset import Dataset
dataset = Dataset(left=to_link, right=reference)
```

We highly recommend to block your data before to link records.
To do so, set up a Blocker
```python
from deduptools.blocker import Blocker
from sklearn.feature_extraction.text import CountVectorizer

blocking_way = [
    {'left_on': 'RAWADDRESS',
     'right_on': 'LIBVOIE',
     'weight': 3,
     'vectorizer': CountVectorizer(ngram_range=(3, 3), analyzer='char_wb', max_df=0.9, min_df=0.05, binary=True)},
    {'left_on': 'LANMENAMES',
     'right_on': 'LIBVOIE',
     'weight': 2,
     'vectorizer': CountVectorizer(ngram_range=(3, 3), analyzer='char_wb', max_df=0.9, min_df=0.05, binary=True)},
    {'left_on': 'POSTCODE',
     'right_on': 'CODPOS',
     'weight': 1,
     'vectorizer': CountVectorizer(ngram_range=(2, 3), analyzer='char_wb', binary=True)},
    {'left_on': 'MUNNAME',
     'right_on': 'LIBCOM',
     'weight': 1,
     'vectorizer': CountVectorizer(ngram_range=(3, 3), analyzer='char_wb', binary=True)}
]

blocker = Blocker(blocking_way=blocking_way, n_jobs=4)

# transform the dataset:
dataset = Blocker.fit_transform(dataset)
```

To link records, you must specify some string comparators (see deduptools.distances)
API assumes the following structure :
```python
import deduptools.distances as strdist
from deduptools.feature_engineering import FeatureCreator

featuring_way = [
    {
        'left_on': 'RAWADDRESS',
        'right_on': 'LIBVOIE',
        'comparator': strdist.LongestCommonSubstring()
    },
    {
        'left_on': 'RAWADDRESS',
        'right_on': 'LIBVOIE',
        'comparator': strdist.WordLevenshteinDistance()
    },
    {
        'left_on': 'RAWADDRESS',
        'right_on': 'LIBVOIE',
        'comparator': strdist.ConvolutionDistance()
    },
    {
        'left_on': 'LANMENAMES',
        'right_on': 'LIBVOIE',
        'comparator': strdist.LongestCommonSubstring()
    },
]

feature_creator = FeatureCreator(featuring_way, n_jobs=4)

#transform the dataset again
dataset = feature_creator.fit_transform(dataset)
```
Note : we highly recommend to implement your own comparators to fit your problem (see distance API for further information)

Then, you might have a trained sklearn-like model for supervised duplicate detection. Such a model must be trained on the same datastructure given by feature_creator.
If you don't, just use the active learning module builtin ```deduptools.active_learning```

```python
from deduptools.active_learning import ActiveLearner
from deduptools.active_learning import query_strategy as qs
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression

# defining strategy pools
start_strategy_pool = [
    qs.RandomMinMax(),
    qs.RandomTopFlop(quantile=0.15),
    qs.RandomSampling()
]

end_strategy_pool = [
    qs.SklearnBoundaryStrategy(RandomForestClassifier, n_estimators=100, max_depth=5),
    qs.SklearnBoundaryStrategy(LogisticRegression)
]

# use ensembling strategy method to randomly altern between pools' 
starting_strats = qs.QueryStrategyEnsemble(start_strategy_pool)
ending_strats = qs.QueryStrategyEnsemble(end_strategy_pool)

active_learner = ActiveLearner(nb_rounds=20)
active_learner.learn(dataset, starting_strats).learn(dataset, ending_strats)
```

Then train a final model and get matches:
```python
model = active_learner.get_sklearn(RandomForestClassifier(n_estimators=100, max_depth=5, n_jobs=-1))

# For deduplication
nb_clusters, memberships = dataset.link(model)

# For record linkage
matches, matches_scores = dataset.get_matches_from(model)
```