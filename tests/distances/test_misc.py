from __future__ import unicode_literals

import pytest

from dedupy.distances.misc import *


def test_exact():
    dist = ExactDistance(modes='all', out_format='tuple')

    assert dist('HELLO WORLD', 'HELLO WORLD') == (.0, .0)
    assert dist('HELLO WORLD', 'GOODBYE WORLD') == (1., 1.)
    assert dist('HELLO WOLRD', '') == (1., 1.)
    assert dist('', '') == (.0, .0)


def test_longest_common_substring():
    dist = LongestCommonSubstring(modes='all', out_format='tuple')

    assert dist('HELLO WORLD', 'HELLO WORLD') == (.0, .0)
    assert dist('HELLO WORLD', 'GOODBYE WORLD') == (13. - 6., pytest.approx((13. - 6.)/13.))
    assert dist('HELLO WOLRD', '') == (11., 1.)
    assert dist('', '') == (.0, .0)


def test_length_difference():
    dist = LengthDifferenceDistance(modes='all', out_format='tuple')

    assert dist('HELLO WORLD', 'HELLO WORLD') == (.0, .0)
    assert dist('HELLO WORLD', 'GOODBYE WORLD') == (13. - 11., pytest.approx((13. - 11.)/13.))
    assert dist('HELLO WOLRD', '') == (11., 1.)
    assert dist('', '') == (.0, .0)


def test_levenshtein():
    dist = LevenshteinDistance(modes='all', out_format='tuple')

    assert dist('HELLO WORLD', 'HELLO WORLD') == (.0, .0)
    assert dist('HELLO WORLD', 'GOODBYE WORLD') == (7., pytest.approx(7./13.))
    assert dist('HELLO WOLRD', '') == (11., 1.)
    assert dist('', '') == (.0, .0)


def test_word_levenshtein():
    dist = WordLevenshteinDistance(modes='all', out_format='tuple')

    assert dist('HELLO WORLD', 'HELLO WORLD') == (.0, .0)
    assert dist('HELLO WORLD', 'GOODBYE WORLD') == (5., pytest.approx(10./(7*(2+2))))
    assert dist('HELLO WOLRD', '') == (15./2., 1.)
    assert dist('', '') == (.0, .0)


def test_convolution():
    dist = ConvolutionDistance(modes='all', out_format='tuple')

    assert dist('HELLO WORLD', 'HELLO WORLD') == (.0, .0)
    assert dist('HELLO WORLD', 'HELLOOWORLD') == (1., pytest.approx(1./11.))
    assert dist('HELLO WOLRD', '') == (11., 1.)
    assert dist('', '') == (.0, .0)
